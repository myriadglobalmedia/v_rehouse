using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wheelRotate : MonoBehaviour
{
    private Driving_Controller dC;
    private static GameObject steeringWheel, forkLift;

    private void RotateWheel()
    {
        steeringWheel.transform.localRotation = Quaternion.Euler(new Vector3(35, -dC.steering,0));
    }

    // Start is called before the first frame update
    void Start()
    {
        steeringWheel = GameObject.Find("SM_SteeringWheel");
        forkLift = GameObject.Find("SM_ForkLift");
        dC = forkLift.GetComponent<Driving_Controller>();
    }

    // Update is called once per frame
    void Update()
    {
        RotateWheel();
    }
}