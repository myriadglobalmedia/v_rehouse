using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pvr_UnitySDKAPI;

    public class ForkLift_Driver_Controller : MonoBehaviour
    {
        public float maxForkTorque = 10f;

        private static GameObject fork, forkLift;

        private Transform forkInitTrans;

        private float forkSpeed;

        private static float forkYPosition;

    private static Quaternion forkInitRotation;

        private GameObject currentController = null;

        private Driving_Controller dC;

    private GameObject GetCurrentController()
        {
            if (currentController == null)
                currentController = FindObjectOfType<Pvr_ControllerDemo>().currentController;
            return currentController;
        }

        private void ForkController()
        {
#if UNITY_EDITOR
        forkSpeed = maxForkTorque * -Input.GetAxis("Up");

        if (Input.GetAxis("Up") > 0 || Input.GetAxis("Up") < 0)
        {
            if (fork.transform.localPosition.y >= forkYPosition && fork.transform.localPosition.y <= 2.3f && dC.steering == 0)
            {
                fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;

                fork.GetComponent<Rigidbody>().isKinematic = false;

                fork.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(0, forkSpeed, 0), Vector3.up, ForceMode.Force);
                fork.transform.localRotation = forkInitRotation;
            }
            else if (fork.transform.localPosition.y >= 2.3f)
            {
                fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

                fork.GetComponent<Rigidbody>().isKinematic = true;

                fork.transform.localPosition = new Vector3(forkInitTrans.localPosition.x, 2.28f, forkInitTrans.localPosition.z);
                fork.transform.localRotation = forkInitRotation;

                fork.GetComponent<Rigidbody>().velocity = Vector3.zero;
                fork.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
            else if (fork.transform.position.y < forkYPosition)
            {
                fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

                fork.GetComponent<Rigidbody>().isKinematic = true;

                fork.transform.localPosition = new Vector3(forkInitTrans.localPosition.x, forkYPosition + 0.05f, forkInitTrans.localPosition.z);
                fork.transform.localRotation = forkInitRotation;

                fork.GetComponent<Rigidbody>().velocity = Vector3.zero;
                fork.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
        }

        if (Input.GetAxis("Up") == 0)
        {
            fork.GetComponent<Rigidbody>().velocity = Vector3.zero;
            fork.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            fork.transform.localRotation = forkInitRotation;

            fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

            fork.GetComponent<Rigidbody>().isKinematic = true;
        }
#elif UNITY_ANDROID
        forkSpeed = maxForkTorque * Controller.UPvr_GetAxis2D(0).y;

        if (GetCurrentController() != null)
        {
            //Debug.Log("I have a controller");
            if (Controller.UPvr_GetAxis2D(0).y > 0 || Controller.UPvr_GetAxis2D(0).y < 0 && dC.steering == 0)
            {
                //Debug.Log("Joystick Y Axis has changed");
                if (fork.transform.localPosition.y >= forkYPosition && fork.transform.localPosition.y <= 2.3f)
                {
                fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;

                fork.GetComponent<Rigidbody>().isKinematic = false;

                fork.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(0, forkSpeed, 0), Vector3.up, ForceMode.Force);
                                //fork.transform.localRotation = forkInitRotation;


                    //Debug.Log("In bounds and moving up or down");
                }
                else if (fork.transform.localPosition.y >= 2.3f)
                {
                fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

                fork.GetComponent<Rigidbody>().isKinematic = true;

                fork.transform.localPosition = new Vector3(forkInitTrans.localPosition.x, 2.28f, forkInitTrans.localPosition.z);
                                //fork.transform.localRotation = forkInitRotation;


                fork.GetComponent<Rigidbody>().velocity = Vector3.zero;
                fork.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                    Debug.Log("Out of bounds upper limit - so I repositioned the forks");
                }
                else if (fork.transform.position.y < forkYPosition)
                {
                fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

                fork.GetComponent<Rigidbody>().isKinematic = true;

                fork.transform.localPosition = new Vector3(forkInitTrans.localPosition.x, forkYPosition + 0.05f, forkInitTrans.localPosition.z);
                                //fork.transform.localRotation = forkInitRotation;


                fork.GetComponent<Rigidbody>().velocity = Vector3.zero;
                fork.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                    //Debug.Log("Out of bounds lower limit - so I repositioned the forks");
                }
            }
        }

        if (GetCurrentController() != null)
        {
            if (Controller.UPvr_GetAxis2D(0).y == 0)
            {
            fork.GetComponent<Rigidbody>().velocity = Vector3.zero;
            fork.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                            //fork.transform.localRotation = forkInitRotation;

            fork.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

            fork.GetComponent<Rigidbody>().isKinematic = true;

                //Debug.Log("immediat stop - Y axis is not moving");
            }
        }
#endif
    }
    // Start is called before the first frame update
    private void Start()
        {
            fork = GameObject.Find("SM_Forks");
            forkInitRotation = fork.transform.localRotation;
            forkYPosition = fork.transform.localPosition.y;
            forkInitTrans = fork.transform;

            forkLift = GameObject.Find("SM_ForkLift");

            dC = forkLift.GetComponent<Driving_Controller>();
        }

        private void FixedUpdate()
        {
            ForkController();

#if UNITY_EDITOR

            if (Input.GetAxis("Vertical") == 0 && GetCurrentController() != null)
            {
                forkLift.GetComponent<Rigidbody>().velocity = Vector3.zero;
                forkLift.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }

#elif UNITY_ANDROID

            if(GetCurrentController() != null && Controller.UPvr_GetAxis2D(1).y == 0)
            {
                forkLift.GetComponent<Rigidbody>().velocity = Vector3.zero;
                forkLift.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            }
#endif
        }
    }