﻿using System.Collections;
using UnityEngine;

public class FollowPath : MonoBehaviour {

    public GameObject wpManager;
    GameObject[] wps;
    UnityEngine.AI.NavMeshAgent agent;

    // Use this for initialization
    void Start() {
        wps = wpManager.GetComponent<WPManager>().waypoints;
        agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();

        StartCoroutine("SetToStart", 1f);
    }

    public void GoToEnd() {
        agent.SetDestination(wps[16].transform.position);
    }

    public void GoToStart() {
        agent.SetDestination(wps[0].transform.position);
    }

    // Update is called once per frame
    void LateUpdate() {

    }

   private IEnumerator End(System.Func<bool> x)
    {
        yield  return new WaitUntil(x);

        GoToEnd();
    }
    
    private IEnumerator Start(System.Func<bool> x)
    {
        yield  return new WaitUntil(x);

        GoToStart();
    }

    private IEnumerator SetToStart(float sec)
    {
        yield return new WaitForSeconds(sec);

        agent.SetDestination(wps[0].transform.position);
    }
}