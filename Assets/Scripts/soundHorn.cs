using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pvr_UnitySDKAPI;

public class soundHorn : MonoBehaviour
{
    private AudioSource horn;
    private static readonly Pvr_KeyCode hornKey = Pvr_KeyCode.A;
    private static GameObject steeringPoint;
    private static Collider l_handCol, r_handCol;
    private GameObject currentController = null;

    private GameObject GetCurrentController()
    {
        if (currentController == null)
            currentController = FindObjectOfType<Pvr_ControllerDemo>().currentController;
        return currentController;
    }

    private static bool GetHornKey()
    {
        return Controller.UPvr_GetKey(0, hornKey) || Controller.UPvr_GetKey(1, hornKey) || Input.GetMouseButtonDown(0);
    }

    private void ForkliftHorn()
    {
        if (GetCurrentController() != null)
        {
            if (GetHornKey())
            {
                if (!horn.isPlaying)
                    horn.Play();
            }
        }
    }
    // Start is called before the first frame update
    private void Start()
    {
        horn = GameObject.Find("Horn").GetComponent<AudioSource>();

        steeringPoint = GameObject.Find("SteeringPoint");

        l_handCol = GameObject.Find("L_Collider").GetComponent<SphereCollider>();
        r_handCol = GameObject.Find("R_Collider").GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    private void Update()
    {
        ForkliftHorn();

        if (steeringPoint.GetComponent<BoxCollider>().bounds.Intersects(r_handCol.bounds) || steeringPoint.GetComponent<BoxCollider>().bounds.Intersects(l_handCol.bounds))
        {
            if (!horn.isPlaying)
                horn.Play();
        }
    }
}
