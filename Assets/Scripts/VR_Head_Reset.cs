using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VR_Head_Reset : MonoBehaviour
{
    public GameObject head, mainObj;

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Driving_Test")
        {
            StartCoroutine("DrivingReset", 0.01f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator DrivingReset(float delay)
    {
        yield return new WaitForSeconds(delay);

        head = GameObject.Find("Head");
        mainObj = GameObject.Find("Pvr_UnitySDK");

        head.transform.localPosition = new Vector3(0, 0, 0);
        head.transform.rotation = new Quaternion(0, 0, 0, 1);

#if UNITY_EDITOR
        mainObj.transform.localPosition = new Vector3(-0.8f, 1.8f, -0.8f);
#elif UNITY_ANDROID
        mainObj.transform.localPosition = new Vector3(-0.8f, 0.5f, -0.8f);
#endif
    }
}
